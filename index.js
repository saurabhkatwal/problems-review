//USING MAP
function addKeyAndValue(ArrayObjs,key,value){
let result=ArrayObjs.map(obj=>{
let temp={};
temp.name=obj.name;
temp[key]=value;
return temp;
})
return result;
}

const arr = [{name: 'Elie'}, {name: 'Tim'}, {name: 'Matt'}, {name: 'Colt'}];
const result=addKeyAndValue(arr,'title','instructor');
console.log(result);


//USING REDUCE
function addKeyAndValue2(ArrayObjs, key, value) {
    let result = ArrayObjs.reduce((acc, obj) => {
        acc.push({
            [key]: value,
            ...obj
        });
        return acc;
    }, [])
    return result;
}

const arr2 = [{
    name: 'Elie'
}, {
    name: 'Tim'
}, {
    name: 'Matt'
}, {
    name: 'Colt'
}];
const result2 = addKeyAndValue2(arr2, 'title', 'instructor');
console.log(result2);





function isEven(value){
    if(value%2===0){
        return true;
    }
    else{
        return false;
    }
}
//PARTITION USING REDUCE
function partition(arr,callback){
    let list1=[];
    let list2=[];

    let res2=arr.reduce((acc,curr)=>{
        if(callback(curr)){
            acc[0].push(curr);
            return acc;
        }
        else{
            acc[1].push(curr);
            return acc;
        }
},[list1,list2]);
 console.log(res2);
}
const values = [1,2,3,4,5,6,7,8];
partition(values,isEven);

//PROMISE CHAIN
const fs = require('fs');
const promise1 = new Promise((resolve, reject) => {
    fs.readFile('./sourceFile/data.txt', 'utf8', (err, data) => {
        if (err) {
            reject(err);
        } else {
            data = data.toUpperCase();
            fs.writeFile("./newFile.txt", data, (err) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("file newFile.txt written successfully");
                    resolve(data);
                }
            })
        }
    })
})
promise1.then(data => {
    return data;
}).then(data => {
    data = data.toLowerCase();
    return data;
}).then(data => {
    data = data.split(" ").sort().join(" ");
    fs.writeFile("sortedDataFile.txt", data, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("file sortedDataFile.txt written successfully");
            let promise2 = new Promise((resolve, reject) => {
                fs.readFile("./sortedDataFile.txt", 'utf8', (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                })
            })
            promise2.then(data => {
                fs.readdir("./", 'utf8', (err, files) => {
                    if (err) {
                        console.log(err);
                    } else {
                        files.filter(file => {
                            return file.includes(".txt");
                        }).forEach(file => {
                            fs.unlink(file, (err) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    console.log("file deleted successfully: " + file);
                                }
                            })
                        })

                    }
                })
            })
        }
    })
})